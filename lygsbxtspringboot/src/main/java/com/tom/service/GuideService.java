package com.tom.service;

import java.util.List;
import java.util.UUID;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.tom.dao.generator.GuideMapper;
import com.tom.model.generator.Guide;
import com.tom.model.generator.GuideExample;
import com.tom.util.ApiResponse;

/**
 * 服务指南service
 * @author tom
 * @since 2019-3-21 16:31:42
 */
@Service("guideService")
public class GuideService {
	@Autowired
	private GuideMapper guideMapper;
	
	public void delGuide(String guideid) {
		Guide guide = getGuide(guideid);
		if(guide != null) {
			guide.setIsdelete(1);
			guideMapper.updateByPrimaryKey(guide);
		}
	}
	
	public Guide getGuide(String guideid) {
		return guideMapper.selectByPrimaryKey(guideid);
	}
	
	@SuppressWarnings("rawtypes")
	public ApiResponse saveGuide(
			String guideid,String title,String content,Integer sort) {
		ApiResponse apiResponsere = new ApiResponse();
		if(title == null || "".equals(title) 
				|| content == null || "".equals(content)
				|| sort == null) {
			apiResponsere.setCode("500");
			apiResponsere.setMsg("请将信息填写完整");
			return apiResponsere;
		}
		Guide guide=null;
		if(guideid != null && !"".equals(guideid)) {
			guide = getGuide(guideid);
		}
		boolean isInsert=false;
		if(guide == null) {
			isInsert=true;
			guide = new Guide();
			guide.setGuideid(UUID.randomUUID().toString());
			guide.setIsdelete(0);
		}
		guide.setTitle(title);
		guide.setContent(content);
		guide.setSort(sort);
		if(isInsert == true) {
			guideMapper.insertSelective(guide);
			apiResponsere.setMsg("新增成功");
		} else {
			guideMapper.updateByPrimaryKey(guide);
			apiResponsere.setMsg("修改成功");
		}
		return apiResponsere;
	}
	
	public List<Guide> getGuideList() {
		GuideExample guideExample = new GuideExample();
		guideExample.createCriteria().andIsdeleteEqualTo(0);
		guideExample.setOrderByClause(" sort asc ");
		return guideMapper.selectByExample(guideExample);
	}

}
