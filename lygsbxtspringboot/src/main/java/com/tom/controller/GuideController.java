package com.tom.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.tom.service.GuideService;
import com.tom.util.ApiResponse;

@RestController
@RequestMapping("/guide")
public class GuideController extends BaseController {
	@Autowired
	private GuideService guideService;
	
	@RequestMapping("/getGuideList")
	public ApiResponse getGuideList() {
		ApiResponse apiResponse = new ApiResponse();
		apiResponse.setData(guideService.getGuideList());
		return apiResponse;
	}
	
	@RequestMapping("/getGuide")
	public ApiResponse getGuide(
			@RequestParam("guideid") String guideid) {
		ApiResponse apiResponse = new ApiResponse();
		apiResponse.setData(guideService.getGuide(guideid));
		return apiResponse;
	}
	
	@RequestMapping("/delGuide")
	public ApiResponse delGuide(
			@RequestParam("guideid") String guideid) {
		guideService.delGuide(guideid);
		ApiResponse apiResponse = new ApiResponse();
		return apiResponse;
	}
	@RequestMapping("/saveGuide")
	public ApiResponse saveGuide(
			@RequestParam("guideid") String guideid,
			@RequestParam("title") String title,
			@RequestParam("content") String content,
			@RequestParam("sort") Integer sort) {
		return guideService.saveGuide(guideid, title, content, sort);
	}
}
