package com.tom.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.tom.model.MaintainorderMaintainuser;
import com.tom.service.MaintainorderService;
import com.tom.util.ExcelUtil;

@Controller
@RequestMapping("/excel")
public class ExcelController {
	@Autowired
	private MaintainorderService maintainorderService;
	
	@RequestMapping("/excelDownloadMaintainorder")
	public void excelDownloadMaintainorder(@RequestParam("status") String status,
			@RequestParam("maintainid") String maintainid,
			HttpServletResponse response) 
			throws IOException{
	    List<MaintainorderMaintainuser> datas = 
	    		maintainorderService.allWebMaintainorder(maintainid, status);
	    List<List<String>> excelData = new ArrayList<>();

	    List<String> head = new ArrayList<>();
	    head.add("标题");
	    head.add("内容");
	    //head.add("图片");
	    head.add("状态");
	    head.add("申请人");
	    head.add("申请时间");
	    head.add("维修人员");
	    head.add("维修费用（元）");
	    head.add("评价得分");
	    head.add("评价内容");
	    excelData.add(head);
	    for(MaintainorderMaintainuser data:datas) {
	    	List<String> row = new ArrayList<String>();
	    	row.add(data.getTitle()==null?"":data.getTitle());
	    	row.add(data.getContent()==null?"":data.getContent());
	    	/*String picture="";
	    	for(String picurl:data.getPictureurls()) {
	    		picture+="https://wechat.lygtc.cn/lygsbxt"+picurl+"\n";
	    	}
	    	row.add(picture);*/
	    	row.add(data.getStatus()==null?"":data.getStatus());
	    	row.add(data.getWxuser().getNickname()==null?"":data.getWxuser().getNickname());
	    	row.add(data.getCreatetimeStr()==null?"":data.getCreatetimeStr());
	    	row.add(data.getMaintainuser().getRealname()==null?"":data.getMaintainuser().getRealname());
	    	row.add(data.getCharge()==null?"":data.getCharge().toString());
	    	row.add(data.getComments().getContent()==null?"":data.getComments().getContent());
	    	excelData.add(row);
	    }
	    String sheetName = "维修工单";
	    String fileName = "维修工单.xls";
	    ExcelUtil.exportExcel(response, excelData, sheetName, fileName, 15);
	}

}
