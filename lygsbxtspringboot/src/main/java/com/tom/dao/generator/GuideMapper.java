package com.tom.dao.generator;

import com.tom.model.generator.Guide;
import com.tom.model.generator.GuideExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface GuideMapper {
    long countByExample(GuideExample example);

    int deleteByExample(GuideExample example);

    int deleteByPrimaryKey(String guideid);

    int insert(Guide record);

    int insertSelective(Guide record);

    List<Guide> selectByExample(GuideExample example);

    Guide selectByPrimaryKey(String guideid);

    int updateByExampleSelective(@Param("record") Guide record, @Param("example") GuideExample example);

    int updateByExample(@Param("record") Guide record, @Param("example") GuideExample example);

    int updateByPrimaryKeySelective(Guide record);

    int updateByPrimaryKey(Guide record);
}