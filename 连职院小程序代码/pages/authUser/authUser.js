var common = require('../../utils/public.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    exit: false,
    visibled: true
  },
  // 用户已授权
  authorizated: function (res) {
    
    var that = this;
    //var userInfo=JSON.parse(wx.getStorageSync('userInfo'));
    var userLogin = wx.getStorageSync('userInfo');
    var userInfo = {};
    if (userLogin) {
      userInfo = JSON.parse(wx.getStorageSync('userInfo'));
    }

    var params = {
      url: '/wxuser/saveWxuser',
      data: { 
         openid: userInfo.openid,
        avatarUrl: res.avatarUrl,
        nickName: res.nickName},
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      successHandler: function (result) {

        if (result.data.code == 200) {
          var userLogin = wx.getStorageSync('userInfo');
          var userInfo = {};
          if (userLogin) {
            userInfo = JSON.parse(wx.getStorageSync('userInfo'));
          }
          userInfo['role'] = 1;
          wx.setStorageSync('userInfo', JSON.stringify(userInfo));
          wx.switchTab({
            url: '/pages/serviceEvaUser/serviceEvaUser',
          })
        } else {
          wx.showToast({
            icon: 'none',
            title: result.data.msg,
          })
        }

      }
    };
    common.ajaxSubmit(params);

  },
  bindGetUserInfo: function (e) {
    if (e.detail.userInfo) {
      //用户按了允许授权按钮
      this.authorizated(e.detail.userInfo)
    } else {
      //用户按了拒绝按钮
      this.setData({
        exit: true
      })
    }
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that=this;
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              // 可以将 res 发送给后台解码出 unionId
              that.authorizated(res.userInfo);

              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res)
              }
            }
          })
        }else{
          that.setData({
            visibled: false
          })
        }
      }
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})