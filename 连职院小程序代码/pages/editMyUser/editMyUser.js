var common = require('../../utils/public.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    nickname:'',
    headurl:'',
    submitHeadurl:''
  },
  bindFormSubmit: function (e) {
    var that = this;
    var nickname = e.detail.value.nickname;
    var openid = JSON.parse(wx.getStorageSync('userInfo')).openid;
    if (nickname== '') {
      wx.showToast({
        icon: 'none',
        title: '标题昵称不能为空',
      });
      return false;
    }
 
    var params = {
      url: '/wxuser/saveWxuser',
      data: {
        openid: openid,
        nickName: nickname,
        avatarUrl: that.data.submitHeadurl,
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      successHandler: function (res) {

        if (res.data.code == 200) {
          wx.switchTab({
            url: '/pages/myUser/myUser',
          })

        } else {
          wx.showToast({
            icon: 'none',
            title: res.data.msg,
          })
        }

      }
    };
    common.ajaxSubmit(params);
  },

  chooseImage: function () {
    var that = this;
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'],
      success: function (res) {

        //获取第一张图片地址 
        var filep = res.tempFilePaths[0]
        //向服务器端上传图片 
        // getApp().data.servsers,这是在app.js文件里定义的后端服务器地址 
        wx.uploadFile({
          url: getApp().globalData.baseUrl + '/upload/uploadFile',
          filePath: filep,
          name: 'file',
          header: {
            "Content-Type": "multipart/form-data",
          },
          success: function (res) {
            if (res.statusCode == 200) {            
              that.setData({
                headurl: 'https://wechat.lygtc.cn/lygsbxt'+res.data,
                submitHeadurl: res.data
              })

            } else {
              wx.showToast({
                icon: 'none',
                title: '上传失败',
              })
            }

          }, fail: function (err) {
            console.log(err)
          }
        });
      }
    })
  },

  loadData: function () {
    var that = this;
    var openid = JSON.parse(wx.getStorageSync('userInfo')).openid;
    var params = {
      url: '/wxuser/getWxuserInfo',
      data: { openid: openid },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      successHandler: function (res) {

        if (res.data.code == 200) {
          var headurl = res.data.data.headurl;
          that.setData({
            submitHeadurl:headurl
          })
          if(headurl.indexOf('https:')<0){
            headurl = 'https://wechat.lygtc.cn/lygsbxt'+headurl;
          }
          that.setData({
            nickname: res.data.data.nickname,
            headurl: headurl
          })

        } else {
          wx.showToast({
            icon: 'none',
            title: res.data.msg,
          })
        }

      }
    };
    common.ajaxSubmit(params);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (app.globalData.oId) {
      this.setData({
        orderId: app.globalData.oId
      })
      this.loadDetail(app.globalData.oId);
    } this.loadData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    app.globalData.oId = null;
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    app.globalData.oId = null;
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})