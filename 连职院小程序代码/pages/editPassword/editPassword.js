var common = require('../../utils/public.js');
var app = getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    
  },
  bindFormSubmit: function (e) {
    var that = this;
    var oldPassword = e.detail.value.oldPassword;
    var newPassword = e.detail.value.newPassword;
    var confirmPassword = e.detail.value.confirmPassword;
    var userInfo = JSON.parse(wx.getStorageSync('userInfo'));
    if (oldPassword == '') {
      wx.showToast({
        icon: 'none',
        title: '原密码不能为空',
      });
      return false;
    }
    if (newPassword == '') {
      wx.showToast({
        icon: 'none',
        title: '新密码不能为空',
      });
      return false;
    }
    if (newPassword != confirmPassword ) {
      wx.showToast({
        icon: 'none',
        title: '两次输入的密码不一致',
      });
      return false;
    }

var url='';
var data={
  oldPassword: oldPassword,
  newPassword: newPassword,
  confirmPassword: confirmPassword,
};
if(userInfo.role==2){
  url ='/maintainuser/changePassword';
  data['maintainid'] = userInfo.userId
}else{
  url = '/officeuser/changePassword';
  data['officerid'] = userInfo.userId
}

    var params = {
      url: url,
      data: data,
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      successHandler: function (res) {

        if (res.data.code == 200) {
          wx.showToast({
            title: '密码修改成功',
          });
          wx.navigateTo({
            url: '/pages/sendMem_list/sendMem_list?role=' + userInfo.role,
          })

        } else {
          wx.showToast({
            icon: 'none',
            title: res.data.msg,
          })
        }

      }
    };
    common.ajaxSubmit(params);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    app.globalData.oId = null;
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    app.globalData.oId = null;
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})