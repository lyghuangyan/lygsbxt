var common = require('../../utils/public.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    curData:'',
    maintainid:'',
    maintainName: '',
    orderId:'',
    role:''
  },
  selectMem: function () {
    wx.navigateTo({
      url: '/pages/selectMem/selectMem',
    })
  },
  prevImg: function (event) {
    
    var src = event.currentTarget.dataset.src;//获取data-src
    var imgList = event.currentTarget.dataset.list;//获取data-list
    for (var i = 0; i < imgList.length; i++) {
      imgList[i] = 'https://wechat.lygtc.cn/lygsbxt' + imgList[i];
    }

    //图片预览
    wx.previewImage({
      current: src, // 当前显示图片的http链接
      urls: imgList // 需要预览的图片http链接列表
    });
  
  },
  loadDetail:function(id){
    var that=this;
    var params = {
      url: '/maintainorder/getMaintainorderDetail',
      data: { orderid: id},
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      successHandler: function (res) {
      
        if (res.data.code==200) {
         
          that.setData({
           curData:res.data.data
          });
          if(that.data.role==2){
            
            that.setData({
              maintainid: res.data.data.maintainid
            });
          }


        }else{
          wx.showToast({
            icon: 'none',
            title: res.data.msg,
          })
        }

      }
    };
    common.ajaxSubmit(params);
  },
  
  sendTask:function(){
    var that = this;
    var userInfo=JSON.parse(wx.getStorageSync('userInfo'));
    
    if (!that.data.maintainid){
      wx.showToast({
        icon:'none',
        title: '请选择维修人员',
      });
      return false;
    }
    var params = {
      url: '/maintainorder/assignMaintainorder',
      data: {
         orderid: that.data.orderId, 
         officerid: userInfo.userId,
        maintainid: that.data.maintainid },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      successHandler: function (res) {

        if (res.data.code == 200) {
          wx.navigateTo({
            url: '/pages/success/success?type=1&title=派单成功',
          })


        } else {
          wx.showToast({
            icon: 'none',
            title: res.data.msg,
          })
        }

      }
    };
    common.ajaxSubmit(params);
  },


  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var orderId=options.orderId;
    var role=options.role;
    this.setData({
      orderId: orderId,
      role: role
    })
    this.loadDetail(orderId);

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    var that = this;
    var pages = getCurrentPages();
    var currPage = pages[pages.length - 1];
    if (currPage.data.maintainid) {
      that.setData({
        maintainid: currPage.data.maintainid,
        maintainName: currPage.data.maintainName,
        role:3
      })
    }
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})