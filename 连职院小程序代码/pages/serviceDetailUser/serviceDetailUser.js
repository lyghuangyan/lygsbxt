var common = require('../../utils/public.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    imgs: [{ src: '/images/example.png' }, { src: '/images/logo.png' }, { src: '/images/logo.png' }, { src: '/images/logo.png' }, , { src: '/images/logo.png' }, , { src: '/images/logo.png' }],
    buildid: '',
    detailData:null,
    evaList: null,
    hasMoreData: true,
    pageNumber: 1,
    pageSize: 10, 
    type:''
  },
  loadDetail:function(buildid){
    var that = this;
    var userInfo = JSON.parse(wx.getStorageSync('userInfo'));
    var data={};
    var url='';
    data['openid'] = userInfo.openid;
    //type=1 服务 type=2 维修员 type=3 工单
    if(that.data.type==1){
      data['buildid']=buildid;
      url ='/building/getBuildingDetail'
    } else if (that.data.type==2){
      data['maintainid'] = buildid;
      url = '/maintainuser/getMaintainuserDetail'
    }
    else if (that.data.type == 3) {
      data['orderid'] = buildid;
      url = '/maintainorder/getMaintainorderDetail'
    }
    var params = {
      url: url,
      data: data,
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      successHandler: function (res) {

        if (res.data.code == 200) {
          var detailInfo = res.data.data;
          if (that.data.type==3){
            if(detailInfo.status=='待评价'){
              detailInfo['hasCommentsQualifications']=true;
            }else{
              detailInfo['hasCommentsQualifications'] = false;
            }
          }
          that.setData({
            detailData: detailInfo
          })

        } else {
          wx.showToast({
            icon: 'none',
            title: res.data.msg,
          })
        }

      }
    };
    common.ajaxSubmit(params);
  },
  loadEvaList:function(message){

    var that = this;
    var url = '/comments/pageComments';
    var data = {
      page: that.data.pageNumber,
      size: that.data.pageSize,
    };
    if(that.data.type==3){
      data['orderid'] = that.data.buildid;
    }else{
      data['relationid'] = that.data.buildid;
    }
  
    common.requestLoading(url, data, message, function (res) {
      

      var courseListTem = that.data.evaList;
      if (res.code) {

        if (that.data.pageNumber == 1) {
          courseListTem = []
        }
        var courseList = res.data.page;
        if (courseList.length < that.data.pageSize) {
          that.setData({
            evaList: courseListTem.concat(courseList),
            hasMoreData: false
          })
        } else {
          that.setData({
            evaList: courseListTem.concat(courseList),
            hasMoreData: true,
            pageNumber: that.data.pageNumber + 1
          })
        }

      } else {

        wx.showToast({
          icon: 'none',
          title: res.msg,
        })
      }
    }, function (res) {
      wx.showToast({
        icon: 'none',
        title: '加载数据失败',
      })

    });

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      var buildid=options.buildid;
      var type=options.type;
      this.setData({
        buildid:buildid,type:type
      });
      this.loadDetail(buildid);
      this.loadEvaList('正在加载数据...');
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.hasMoreData) {
      this.loadEvaList('加载更多数据');
    } else {

      wx.showToast({
        icon: 'none',
        title: '没有更多数据',
      })
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})