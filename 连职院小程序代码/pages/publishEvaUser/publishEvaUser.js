var common = require('../../utils/public.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    buildid:'',
    type:'',
    stars:1
  },
  changeStar:function(e){
    var index=e.target.dataset.index;
    this.setData({
      stars:index
    })
    
  },
  bindFormSubmit:function(e){
    var that=this;
     var userInfo=JSON.parse(wx.getStorageSync('userInfo')) 
     var content=e.detail.value.content;
     var data={};
     var url='';
     data['content']=content;
     data['stars']=this.data.stars;
     data['openid']=userInfo.openid;
     //type=1 评价服务 type=2 评价维修人员 type=3 评价工单
     if(this.data.type==1){
       data['buildid']=this.data.buildid;
       url = '/comments/commentBuilding'
     } else if (this.data.type == 2){
       data['maintainid'] = this.data.buildid;
       url ='/comments/commentMaintainuser';
     } else if (this.data.type == 3) {
       data['orderid'] = this.data.buildid;
       url = '/comments/commentOrder';
     }

    var params = {
      url: url,
      data: data,
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      successHandler: function (res) {

        if (res.data.code == 200) {
          if (that.data.type == 3){
            wx.redirectTo({
              url: '/pages/success/success?type=5&title=评价成功',
            })
          }else{
            wx.redirectTo({
              url: '/pages/success/success?type=2&title=评价成功',
            })
          }
         

        } else {
          wx.showToast({
            icon: 'none',
            title: res.data.msg,
          })
        }

      }
    };
    common.ajaxSubmit(params);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      var buildid=options.buildid;
      var type=options.type;
      this.setData({
        buildid:buildid,
        type:type
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})