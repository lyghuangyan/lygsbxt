var common = require('../../utils/public.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    nickname:'',
    headurl:''
  },
  myReport:function(){
    wx.navigateTo({
      url: '/pages/myMaintainReport/myMaintainReport',
    })
  },
  systemSet: function () {
    wx.navigateTo({
      url: '/pages/set/set?type=1',
    })
  },
  aboutUs:function(){
    wx.navigateTo({
      url: '/pages/aboutUs/aboutUs',
    })
  },
  loadData:function(){
    var that=this;
    var openid=JSON.parse(wx.getStorageSync('userInfo')).openid;
    var params = {
      url: '/wxuser/getWxuserInfo',
      data: { openid: openid},
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      successHandler: function (res) {

        if (res.data.code == 200) {
          var headurl = res.data.data.headurl;
          if (headurl.indexOf('https:') < 0) {
            headurl = 'https://wechat.lygtc.cn/lygsbxt' + headurl;
          }
          that.setData({
            nickname: res.data.data.nickname,
            headurl: headurl
          })

        } else {
          wx.showToast({
            icon: 'none',
            title: res.data.msg,
          })
        }

      }
    };
    common.ajaxSubmit(params);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
      this.loadData();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.loadData();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})