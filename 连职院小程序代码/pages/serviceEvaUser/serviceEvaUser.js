var common = require('../../utils/public.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    serviceFirst:null,
    serviceList:[],
    maintainList:[]

  },
  jumpDetail:function(e){
    var buildid = e.currentTarget.dataset.buildid;
    var type=e.currentTarget.dataset.type;
    wx.navigateTo({
      url: '/pages/serviceDetailUser/serviceDetailUser?buildid=' + buildid+'&type='+type ,
    })
  },
  goComment:function(e){
    var buildid=e.target.dataset.buildid;
    var type = e.target.dataset.type;
    wx.navigateTo({
      url: '/pages/publishEvaUser/publishEvaUser?buildid='+buildid+'&type='+type,
    })
  
  },
  loadList:function(){
    var that = this;
    var userInfo = JSON.parse(wx.getStorageSync('userInfo'));
    var params = {
      url: '/wxuser/wxuserIndex',
      data: { openid: userInfo.openid },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      successHandler: function (res) {

        if (res.data.code == 200) {
          that.setData({
            serviceFirst: res.data.data.buildingtypeWithBuildings[0],
            serviceList: res.data.data.buildingtypeWithBuildings.slice(1),
            maintainList: res.data.data.maintainuserWithCommentsQualificationsList
          })

        } else {
          wx.showToast({
            icon: 'none',
            title: res.data.msg,
          })
        }

      }
    };
    common.ajaxSubmit(params);

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   this.loadList();
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.loadList();
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})