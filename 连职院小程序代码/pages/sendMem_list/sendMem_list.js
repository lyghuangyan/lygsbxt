var common = require('../../utils/public.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    hasMoreData: true,
    pageNumber: 1,
    pageSize: 10,
    status:'未派单',
    curIndex:0,
    role:'',
  },
  change: function (e) {
    var status = e.target.dataset.type;
    var index=0;
    if(status=='未派单' || status=='未完成'){
        index=0
    } else if (status == '已派单' || status == '已完成'){
      index=1;
    }
    this.setData({
      status: status,
      hasMoreData: true,
      pageNumber: 1,
      curIndex:index

    })
    this.loadList('正在加载数据...');

  },
  loadList: function (message) {
    var that = this;
    var role =that.data.role;
    var url ='/maintainorder/pageOfficeuserMaintainorder';
    var data = {
      page: that.data.pageNumber,
      size: that.data.pageSize,
      status:that.data.status
    };
    if (role == 2) {
      data['maintainid']=JSON.parse(wx.getStorageSync('userInfo')).userId;
      url ='/maintainorder/pageMaintainuserMaintainorder';
    }
    common.requestLoading(url, data, message, function (res) {


      var courseListTem = that.data.list;
      if (res.code) {

        if (that.data.pageNumber == 1) {
          courseListTem = []
        }
        var courseList = res.data.page;
        if (courseList.length < that.data.pageSize) {
          that.setData({
            list: courseListTem.concat(courseList),
            hasMoreData: false
          })
        } else {
          that.setData({
            list: courseListTem.concat(courseList),
            hasMoreData: true,
            pageNumber: that.data.pageNumber + 1
          })
        }

      } else {

        wx.showToast({
          icon: 'none',
          title: res.msg,
        })
      }
    }, function (res) {
      wx.showToast({
        icon:'none',
        title: '加载数据失败',
      })

    });

  },
  call: function (e) {
    var phone = e.target.dataset.phone;
    wx.makePhoneCall({
      phoneNumber: phone// 仅为示例，并非真实的电话号码
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var role=options.role;
    if(role==2){
      this.setData({
        role: role,
        status: '未完成'
      })
    }else{
      this.setData({
        role: role,
        status: '未派单'
      })
    }
    
    this.loadList('正在加载数据...');
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
    if (this.data.hasMoreData) {
      this.loadList('加载更多数据');
    } else {

      wx.showToast({
        icon:'none',
        title: '没有更多数据',
      })
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})