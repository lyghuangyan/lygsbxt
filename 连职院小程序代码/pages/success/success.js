// pages/success/success.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    type:''
  },
  goSendTask:function(){
    wx.redirectTo({
      url: '/pages/sendMem_list/sendMem_list?role=3',
    })
  },
  back:function(){
    wx.redirectTo({
      url: '/pages/sendMem_list/sendMem_list?role=2',
    })
  },
  backIndex:function(){
    
    wx.switchTab({
      url: '/pages/serviceEvaUser/serviceEvaUser',
    })
  },
  writeReport: function () {
    
    wx.switchTab({
      url: '/pages/maintainReportUser/maintainReportUser',
    })
  },
  myReport:function(){
    wx.switchTab({
      url: '/pages/myUser/myUser',
    })
  },
  backMyList:function(){
    wx.switchTab({
      url: '/pages/myUser/myUser',
    })
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    /**
     * type = 1 派单成功
     * type = 2 评价成功
     * type = 3 申报成功
     * type = 4 确认完成
     * type = 5  评价工单完成
     */
     var type=options.type;
      wx.setNavigationBarTitle({
      title: options.title//页面标题为路由参数
      });
      this.setData({
        type:type
      })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})