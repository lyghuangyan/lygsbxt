var common = require('../../utils/public.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    role:''
  },
  login:function(e){
    var username = e.detail.value.username;
    var password = e.detail.value.password;
   
    var that = this;
    var role=that.data.role;
    var url='';
    if(role==1){
      
    }else if(role==2){
      url = '/maintainuser/login'
    }else{
      url = '/officeuser/login'
    }
    var para = {
      url: url,
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
     
      data: {
        username:username,
        password:password
      },
      successHandler: function (res) {
        if (res.data.code==200) {
          var userLogin = wx.getStorageSync('userInfo');
          var userInfo ={};
          if (userLogin){
            userInfo = JSON.parse(wx.getStorageSync('userInfo'));
          }
          
         var jumpUrl='';
          if(role==3){
            userInfo['userId'] = res.data.data.officerid;
            jumpUrl = '/pages/sendMem_list/sendMem_list?role=3';
          }else if(role==2){
            userInfo['userId'] = res.data.data.maintainid;
            jumpUrl = '/pages/sendMem_list/sendMem_list?role=2';
          }else{
            
          }
          
          userInfo['role']=role;
          wx.setStorageSync('userInfo',JSON.stringify(userInfo));
          wx.reLaunch({
            url: jumpUrl,
          })
          

        } else {
          wx.showToast({
            icon: 'none',
            title: res.data.msg,
          })
        }
      }
    };
    common.ajaxSubmit(para);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
   
    /**
     * role=1 用户
     * role=2 维修人员
     * rol3=3 我是派单员
     */
    var role=options.role;
    this.setData({
      role:role
    })

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})