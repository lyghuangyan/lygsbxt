//index.js
//获取应用实例
const app = getApp()

Page({
  data: {
   screenH:''
  },

  onLoad: function () {
    var that=this;
    wx.getSystemInfo({
      success(res) {
        that.setData({
          screenH: res.windowHeight-30
        })
      }
    })
  },
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },
  
})
