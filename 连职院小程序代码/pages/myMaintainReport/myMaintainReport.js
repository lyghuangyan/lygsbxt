var common = require('../../utils/public.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    list: [],
    hasMoreData: true,
    pageNumber: 1,
    pageSize: 10,
    status: '',
    day: -1,
    status_list: [{
      type: '',
      text: '全部状态'
    }, {
      type: '未派单',
      text: '未派单'
    }, {
      type: '维修中',
      text: '维修中'
    }, {
      type: '待评价',
      text: '待评价'
    }, {
      type: '已完成',
      text: '已完成'
    }],
    status_index: 0,
    day_list: [{
      type: -1,
      text: '全部时间'
    }, {
      type: 0,
      text: '只看今天'
    }, {
      type: 3,
      text: '最近三天'
    }, {
      type: 7,
      text: '最近一周'
    }, {
      type: 30,
      text: '最近一月'
    }],
    day_index: 0
  },
  call:function(e){
    var phone=e.target.dataset.phone;
    wx.makePhoneCall({
      phoneNumber: phone// 仅为示例，并非真实的电话号码
    })
  },
  jumpDetail: function (e) {
    var buildid = e.currentTarget.dataset.buildid;
    var type = e.currentTarget.dataset.type;
    wx.navigateTo({
      url: '/pages/serviceDetailUser/serviceDetailUser?buildid=' + buildid + '&type=' + type,
    })
  },
  jumpComment:function(e){
    var buildid = e.currentTarget.id;
    wx.navigateTo({
      url: '/pages/publishEvaUser/publishEvaUser?type=3&buildid='+buildid,
    })
  },
  editData:function(e){
    var that = this;
    var orderid = e.currentTarget.id;
    getApp().globalData.oId=orderid;
    wx.switchTab({
      url: '/pages/maintainReportUser/maintainReportUser',
      success: function (e) {
        var page = getCurrentPages().pop();
        page.onLoad();
      }
    })
  },
  delData:function(e){
    var that=this;
    var orderid=e.currentTarget.id;
    //删除数据
    wx.showModal({
      title: '温馨提示',
      content: '是否确认删除',
      cancelColor:'#878787',
      confirmColor:'#33A9FE',
      success(res) {
        if (res.confirm) {
          var params = {
            url: '/maintainorder/delMaintainorder',
            data: { orderid: orderid },
            header: {
              'content-type': 'application/x-www-form-urlencoded'
            },
            successHandler: function (res) {
             
              if (res.data.code == 200) {
                wx.showToast({
                  title: '删除成功',
                });
                that.loadList('正在加载数据...');

              } else {
                wx.showToast({
                  icon: 'none',
                  title: res.data.msg,
                })
              }

            }
          };
          common.ajaxSubmit(params);
        } else if (res.cancel) {
          
        }
      }
    })
  },
  dayChange: function(e) {
    var index = e.detail.value;
    var day = this.data.day_list[index].type; // 这个id就是选中项的id

    this.setData({
      day_index: e.detail.value,
      day: day,
      pageNumber: 1,
      hasMoreData: true,
    });
    this.loadList('正在加载数据...');
  },
  statusChange: function(e) {
    var index = e.detail.value;
    var status = this.data.status_list[index].type; // 这个id就是选中项的id

    this.setData({
      status_index: e.detail.value,
      status: status,
      pageNumber: 1,
      hasMoreData: true,
    });
    this.loadList('正在加载数据...');
  },
  loadList: function(message) {
    var that = this;
    var role = that.data.role;
    var openid = JSON.parse(wx.getStorageSync('userInfo')).openid;
    var data = {
      page: that.data.pageNumber,
      size: that.data.pageSize,
      status: that.data.status,
      days: that.data.day,
      openid: openid
    };

    common.requestLoading('/maintainorder/pageWxuserMaintainorder', data, message, function(res) {


      var courseListTem = that.data.list;
      if (res.code) {

        if (that.data.pageNumber == 1) {
          courseListTem = []
        }
        var courseList = res.data.page;
        if (courseList.length < that.data.pageSize) {
          that.setData({
            list: courseListTem.concat(courseList),
            hasMoreData: false
          })
        } else {
          that.setData({
            list: courseListTem.concat(courseList),
            hasMoreData: true,
            pageNumber: that.data.pageNumber + 1
          })
        }

      } else {

        wx.showToast({
          icon: 'none',
          title: res.msg,
        })
      }
    }, function(res) {
      wx.showToast({
        icon: 'none',
        title: '加载数据失败',
      })

    });

  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.loadList('正在加载数据...');

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {   
      /*wx.reLaunch({
        url: '/pages/myUser/myUser'
      })*/
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {
    if (this.data.hasMoreData) {
      this.loadList('加载更多数据');
     
    } else {

      wx.showToast({
        icon: 'none',
        title: '没有更多数据',
      })
    }
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})