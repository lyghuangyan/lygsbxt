var common = require('../../utils/public.js');
var app=getApp();
Page({

  /**
   * 页面的初始数据
   */
  data: {
    orderId:'',
    detailData:'',
    imgs:[],
    imgCount:0,
    is_chooseImg:false
  },
  bindFormSubmit:function(e){
    var that=this;
    var title=e.detail.value.title;
    var address = e.detail.value.address; 
    var content=e.detail.value.content;
    var openid=JSON.parse(wx.getStorageSync('userInfo')).openid;
    var pictureurls=that.data.imgs;
    if (title == '') {
      wx.showToast({
        icon: 'none',
        title: '标题不能为空',
      });
      return false;
    }
    if (address == '') {
      wx.showToast({
        icon: 'none',
        title: '地点不能为空',
      });
      return false;
    }
    if (content == '') {
      wx.showToast({
        icon: 'none',
        title: '内容不能为空',
      });
      return false;
    }
    if(pictureurls.length==0){
      wx.showToast({
        icon: 'none',
        title: '请上传照片',
      });
      return false;
    }
    pictureurls = pictureurls.toString();
    var params = {
      url: '/maintainorder/submitMaintainorder',
      data:{
        openid: openid,
        title: title,
        address: address,
        content: content,
        'pictureurls[]': pictureurls,
        orderid: that.data.orderId
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      method: 'POST',
      successHandler: function (res) {

        if (res.data.code == 200) {
          wx.redirectTo({
            url: '/pages/success/success?type=3&title=提交成功',
          });
          app.globalData.oId = null;

        } else {
          wx.showToast({
            icon: 'none',
            title: res.data.msg,
          })
        }

      }
    };
    common.ajaxSubmit(params);
  },
  removeImg:function(e){
    var index=e.currentTarget.dataset.index;
    var imgs=this.data.imgs;
    imgs.splice(index,1);
    var count=imgs.length;
    this.setData({
      imgs:imgs,
      imgCount:count
    })
   
  },
  chooseImage: function () {
    var that = this;
    that.setData({
      is_chooseImg:true
    })
    wx.chooseImage({
      count: 1, // 默认9
      sizeType: ['original', 'compressed'], // 可以指定是原图还是压缩图，默认二者都有
      sourceType: ['album', 'camera'],
      success: function (res) {
      
        //获取第一张图片地址 
        var filep = res.tempFilePaths[0]
        //向服务器端上传图片 
        // getApp().data.servsers,这是在app.js文件里定义的后端服务器地址 
        wx.uploadFile({
          url: getApp().globalData.baseUrl+'/upload/uploadFile',
          filePath: filep,
          name: 'file',
          header: {
            "Content-Type": "multipart/form-data",  
          },
          success: function (res) {
            if(res.statusCode==200){
              var imgs=that.data.imgs;
              imgs.push(res.data);
              var count=imgs.length;
              that.setData({
                imgs:imgs,
                imgCount:count,
                is_chooseImg:false
              })

            }else{
              wx.showToast({
                icon: 'none',
                title: '上传失败',
              })
            }
            

             
          }, fail: function (err) {
            
          }
        });
      }
    })
  }, 
  loadDetail:function(orderid){
    var that=this;
    var params = {
      url: '/maintainorder/getMaintainorderDetail',
      data: {
        orderid:orderid
      },
      header: {
        'content-type': 'application/x-www-form-urlencoded'
      },
      successHandler: function (res) {

        if (res.data.code == 200) {
          that.setData({
            detailData:res.data.data,
            imgs: res.data.data.pictureurls,
            imgCount: res.data.data.pictureurls.length
          })

        } else {
          wx.showToast({
            icon: 'none',
            title: res.data.msg,
          })
        }

      }
    };
    common.ajaxSubmit(params);
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    if (app.globalData.oId){
      this.setData({
        orderId: app.globalData.oId
      })
      this.loadDetail(app.globalData.oId);
    }
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
    
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
    app.globalData.oId=null;
    if(!this.data.is_chooseImg){
      this.setData({
        orderId: '',
        detailData: '',
        imgs: [],
        imgCount: 0
      })
    }
    
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    app.globalData.oId=null;
    if (!this.data.is_chooseImg) {
      this.setData({
        orderId: '',
        detailData: '',
        imgs: [],
        imgCount: 0
      })
    }
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    wx.stopPullDownRefresh();
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})