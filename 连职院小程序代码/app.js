//app.js
var common = require('utils/public.js');
App({
  onLaunch: function (options) {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs);
    var userLogin = wx.getStorageSync('userInfo');
    if (userLogin){
      var userInfo = JSON.parse(wx.getStorageSync('userInfo'));
      var role=userInfo.role;
      if(role){
        if (role == 1) {
          wx.switchTab({
            url: '/pages/serviceEvaUser/serviceEvaUser',
          })

        } else {
          if (userInfo.userId) {
            wx.redirectTo({
              url: '/pages/sendMem_list/sendMem_list?role=' + role,
            })
          }
        }
        return;
      }
    }

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        if (res.errMsg == "login:ok") {
          var params = {
            url: '/wechat/loginWithCode',
            data: { code: res.code},
            method: 'GET',
            header:{
              'content-type': 'application/x-www-form-urlencoded'
            },
            successHandler: function (res) {
             
              if (res.data.code==200) {
                var userInfo = {
                  openid: res.data.data.openid,
                 
                };
                wx.setStorageSync('userInfo', JSON.stringify(userInfo));
                wx.redirectTo({
                  url: '/pages/selectRole/selectRole',
                })
              }else{
                wx.showToast({
                  icon:'none',
                  title: res.data.message,
                })
              }

            }
          };
          common.ajaxSubmit(params);
        }
        
      }
    })
    
  },
  onPageNotFound:function(e){
    console.log(e)
  },
  globalData: {
    baseUrl: 'https://wechat.lygtc.cn/lygsbxt',
    scene:1
  }
})